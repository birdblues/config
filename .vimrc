set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'tomasr/molokai'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
Plugin 'L9'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
" Plugin 'user/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

set backspace=2     " 삽입 모드에서 백스페이스를 계속 허용
"set autoindent      " 자동 들여쓰기
"set cindent         " C 언어 자동 들여쓰기
"set smartindent     " 역시 자동 들여쓰기
"set textwidth=76    " 76번째 칸을 넘어가면 자동으로 줄 바꿈
"set nowrapscan      " 찾기에서 파일의 맨 끝에 이르면 계속하여 찾지 않음
set nobackup       " 백업파일을 만들지 않음
set novisualbell    " 비주얼벨 기능을 사용하지 않음
"set nojoinspaces    " J 명령어로 줄을 붙일 때 마침표 뒤에 한칸만 띔
"set ruler           " 상태표시줄에 커서 위치를 보여줌
set tabstop=2       " <Tab> 간격
set shiftwidth=2    " 자동 들여쓰기 간격
set keywordprg=edic    " K를 눌렀을 때 실행할 명령어
set showcmd         " (부분적인) 명령어를 상태라인에 보여줌
set showmatch       " 매치되는 괄호의 반대쪽을 보여줌
set ignorecase      " 찾기에서 대/소문자를 구별하지 않음
set incsearch       " 점진적으로 찾기
set autowrite       " :next 나 :make 같은 명령를 입력하면 자동으로 저장
set title           " 타이틀바에 현재 편집중인 파일을 표시
"set hlsearch        " 찾는 단어를 하이라이팅

set nu
syn on

syntax enable
set background=dark
colorscheme Tomorrow-Night-Bright

"
" UTF-8
set encoding=utf8
set fileencoding=utf8
set fileencodings=ucs-bom,utf-8,euc-kr,latin1

"
" EUC-KR
"set encoding=euc-kr
"set fileencoding=korea
"
"
"

map <F5> <ESC>:s/^\(.\+\)$/\/* \1 *\//<CR>:noh<CR>
map <F6> <ESC>:gr '\<<C-R><C-W>\>' -R *<CR>
map <F7> <ESC>:gr '\<<C-R><C-W>\>' `find ./ -name "*.h"`<CR>
map <F8> <ESC>:gr '\<<C-R><C-W>\>' `find ./ -name "*.c"`<CR>
map <F9> <ESC>:gr '\<<C-R><C-W>\>' `find ./ -name "*.cpp"`<CR>
map <F10> <ESC>:cp<CR>
map <F11> <ESC>:cl<CR>
map <F12> <ESC>:cn<CR>

nnoremap <C-B> :YcmCompleter GoTo<CR>

if &diff

	set diffopt=filler,context:1000000 " filler is default and inserts empty lines for sync

	" When using vimdiff or diff mode
	highlight DiffAdd    term=bold         ctermbg=22 ctermfg=white  cterm=bold guibg=DarkGreen  guifg=White    gui=bold
	highlight DiffText   term=reverse,bold ctermbg=88 ctermfg=yellow cterm=bold guibg=DarkRed    guifg=yellow   gui=bold
	highlight DiffChange term=bold         ctermbg=black ctermfg=white  cterm=bold guibg=Black      guifg=White    gui=bold
	highlight DiffDelete term=none         ctermbg=52  ctermfg=52 cterm=none guibg=DarkBlue   guifg=DarkBlue gui=none

	" When viewing a diff or patch file
	highlight diffRemoved term=bold ctermbg=black   ctermfg=red    cterm=bold guibg=DarkRed     guifg=white gui=none
	highlight diffAdded   term=bold ctermbg=black   ctermfg=green  cterm=bold guibg=DarkGreen   guifg=white gui=none
	highlight diffChanged term=bold ctermbg=black   ctermfg=yellow cterm=bold guibg=DarkYellow  guifg=white gui=none
	highlight diffLine    term=bold ctermbg=magenta ctermfg=white  cterm=bold guibg=DarkMagenta guifg=white gui=none
	highlight diffFile    term=bold ctermbg=yellow  ctermfg=black  cterm=none guibg=DarkYellow  guifg=white gui=none

	nnoremap <silent> <C-Up> [c
	nnoremap <silent> <C-Down> ]c
	nnoremap <silent> <C-Right> <c-w>l
	nnoremap <silent> <C-Left> <c-w>h
	nnoremap <silent> <C-S-Left> <ESC>:diffput<CR>
	nnoremap <silent> <C-S-Right> <ESC>:diffput<CR>

endif
