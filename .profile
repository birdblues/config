# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n
export GREP_OPTIONS='--color=auto'
export USE_CCACHE=1
export CCACHE_DIR=~/.ccache
export PROMPT_DIRTRIM=3
source ~/git-completion.bash
